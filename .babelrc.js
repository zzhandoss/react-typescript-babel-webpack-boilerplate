module.exports = {
    preset: ["@babel/preset-react", "@babel/preset-env", "@babel/preset-typescript"],
    plugins: [
        "react-refresh/babel"
    ]
}